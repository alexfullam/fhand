# FHand
README

FHAND

-------------
Documentation
-------------

alexfullam.wordpress.com/fhand


-----------------------------
Command Line Parameters/Flags
-----------------------------

usage: fhand [-h] [-t] [-e] [-d] [-wc] [-del] [-rs] [-cl] [-fc] [-l] [-rew]
             [-c] [-ht] [-tip] [-tip12] [-tic] [-tic12]


optional arguments:
  -h, --help            show the help message and exit

  -t, --texteditor      Create or edit a text file (Opens new window with GUI)

  -e, --encrypt         Encrypt file, entering a secret code.
  
  -d, --decrypt         Decrypt a file you encrypted with -e by entering the
                        secret code you encrypted it with.

  -wc, --wordcount      This is what you'll see: How many OF EACH WORD is in a
                        file, how MANY WORDS THERE ARE in the file, and how
                        many CHARACTERS there are in the file.

  -del, --delete        Move a file to the trash.

  -rs, --read           Print out the contents of a text file into the shell.

  -cl, --clear          Clear the contents of a file. (Behind the scenes:
                        Truncates the file by setting it to 0 bytes)

  -fc, --filecheck      Check if the given path is a regular file, directory,
                        or character device.

  -l, --list            Get a list of all files inside a directory, AND inside
                        subdirectories. You may also choose specific file
                        types. Hidden files are included.

  -rew, --removeexcesswhitespace
                        Replace multiple spaces with a single space in a file.
                        (Remove excess whitespace.) Output file is stored in
                        home directory.

  -c, --createdirectory Create a directory

  -ht, --helptime       Get help with the time functions.

  -tip, --timeprint     Prints the time. (24-hour)

  -tip12, --timeprint12
                        Prints the time (12 hour)

  -tic, --clock         Returns the time (clock mode) WARNING: This uses the
                        shell command 'clear'. If you have anything else
                        printed in the shell, it will be cleared.
                        
  -tic12, --clock12     Returns the time (clock mode, 12-hour) Warning: uses
                        shell command 'clear. If you have anything else
                        printed in the shell, it will be cleared.'



------------
INSTALLATION
------------

To install FHand, you first must have these installed:

- python3
- python3-pip
To install python3-pip on Debian-based Linux distros, use:

```sudo apt install python3-pip```

Download the Pyton file.

Next, if you’re using the FHand file, drop it into wherever your terminal programs are run from (possibly /bin or /usr/bin). You might have to get root permissions to do that. Next, you have to make sure you have permission to run the file. Run:

```chmod 777 /usr/bin/FHand```
This will allow all users to read, write, and execute FHand.

You can now run FHand by just typing FHand into the command line. If you have problems, leave a comment at alexanderfullam.com/fhand



